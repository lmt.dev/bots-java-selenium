package com.stcor.ScraperBot;

import com.stcor.ScraperBot.Services.BotService;
import com.stcor.ScraperBot.Services.CordobaVende_Service;
import com.stcor.ScraperBot.Services.OLX_Service;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@SpringBootApplication
public class ScraperBotApplication implements CommandLineRunner {
	String selectedService = "";

	public static void main(String[] args) {
		SpringApplication.run(ScraperBotApplication.class, args);

	}
	@Bean
	public BotService getBotService(){
		switch (selectedService){
			case "CordobaVende_Service":
				return new CordobaVende_Service();
			default:
				return new OLX_Service();
		}
	}

	@Override
	public void run(String... args) throws Exception {
		getBotService().start();
	}

}

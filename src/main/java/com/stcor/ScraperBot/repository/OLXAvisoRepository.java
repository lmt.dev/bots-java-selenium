/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stcor.ScraperBot.repository;

import com.stcor.ScraperBot.Bots.model.olx.OLXAviso;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Lucas
 */
@Repository
public interface OLXAvisoRepository extends JpaRepository<OLXAviso,String>{
    List<OLXAviso> findByDescartado(String descartado);
}
